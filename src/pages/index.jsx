import React from 'react'
import { Container, Grid } from '@material-ui/core'
import Layout from '../layouts'
import { useProducts } from '../hooks'
import Product from '../components/Product/'

const IndexPage = () => {
  const { products } = useProducts()

  return (
    <Layout>
      <Container maxWidth="md">
        <Grid container spacing={4} justifyContent="center">
          {products.map((product) => (
            <Grid item xs={10} sm={6} md={4} key={product.id}>
              <Product {...product} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </Layout>
  )
}
export default IndexPage
