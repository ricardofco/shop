import { Button } from '@material-ui/core'
import React from 'react'
import PropTypes from 'prop-types'

const BuyButton = (props) => {
  const { id, price, image, name, url, children, ...rest } = props
  return (
    <Button
      variant="contained"
      fullWidth
      className="snipcart-add-item"
      color="secondary"
      data-item-id={id}
      data-item-price={price}
      data-item-image={image.url}
      data-item-name={name}
      data-item-url={url}
      {...rest}
    >
      {children || 'Buy Now'}
    </Button>
  )
}

BuyButton.propTypes = {
  children: PropTypes.string
}

export default BuyButton
