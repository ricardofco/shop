import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  bold: {
    fontWeight: 'bold'
  },
  productImg: {
    height: 250
  }
}))

export default useStyles
