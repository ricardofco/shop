import { Container } from '@material-ui/core'
import { graphql } from 'gatsby'
import React from 'react'
import Layout from '../../../layouts'

export const query = graphql`
  query ($slug: String!) {
    products: allDatoCmsProduct(filter: { slug: { eq: $slug } }) {
      nodes {
        id
        name
        price
        image {
          url
          sizes(maxWidth: 300, imgixParams: { fm: "jpg" }) {
            ...GatsbyDatoCmsSizes
          }
        }
      }
    }
  }
`

const ProductDetail = ({
  data: {
    products: { nodes }
  }
}) => {
  return (
    <Layout>
      <Container>
        <pre>{JSON.stringify(nodes[0], null, 4)}</pre>
      </Container>
    </Layout>
  )
}

export default ProductDetail
