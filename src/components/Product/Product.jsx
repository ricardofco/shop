import React from 'react'
import Img from 'gatsby-image'
import { Box, Card, Typography } from '@material-ui/core'
import { Link } from 'gatsby'
import useStyles from './ProductStyles'
import BuyButton from '../BuyButton'

const Product = (props) => {
  const { id, name, price, slug, image } = props
  const classes = useStyles()

  return (
    <Card>
      <Link to={slug}>
        <Img fluid={image.sizes} className={classes.productImg} />
      </Link>
      <Box>
        <Typography variant="subtitle1" gutterBottom>
          {name}
        </Typography>
        <Typography variant="h6" className={classes.bold}>
          $ {price}
        </Typography>
        <BuyButton
          id={id}
          price={price}
          image={image.url}
          name={name}
          url={`/`}
        />
      </Box>
    </Card>
  )
}

export default Product
