import { graphql, useStaticQuery } from 'gatsby'

const useProducts = () => {
  const data = useStaticQuery(graphql`
    query CatalogueQuery {
      products: allDatoCmsProduct {
        nodes {
          id
          name
          price
          slug
          image {
            url
            sizes(maxWidth: 300, imgixParams: { fm: "jpg" }) {
              ...GatsbyDatoCmsSizes
            }
          }
        }
      }
    }
  `)
  return {
    products: data.products.nodes
  }
}
export default useProducts
