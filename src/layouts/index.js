import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { Typography, AppBar, Toolbar, Link, Box } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import './layout.scss'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  cart: {
    color: theme.palette.common.white
  },
  main: {
    marginTop: theme.spacing(3)
  }
}))

const Layout = ({ children, site }) => {
  const classes = useStyles()
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            My shop
          </Typography>
          <Link
            to="#"
            className={clsx(classes.cart, 'snipcart-summary snipcart-checkout')}
          >
            <Box>🛍 MY CART 🛍</Box>
            <Typography>
              Number of items:
              <Typography
                variant="overline"
                className="snipcart-total-items"
              ></Typography>
            </Typography>
            <Typography>
              Total price:
              <Typography
                variant="overline"
                className="snipcart-total-price"
              ></Typography>
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
      <Helmet title="Snipcart + DatoCMS + GatsbyJS Example" />
      <Box className={classes.main}>{children}</Box>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node
}

export default Layout
