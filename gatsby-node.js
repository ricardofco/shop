exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const result = await graphql(`
    query {
      products: allDatoCmsProduct {
        nodes {
          slug
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panic('Pagination error: ', result.errors)
  }

  const products = result.data.products.nodes

  products.forEach(({ slug }) => {
    createPage({
      path: slug,
      component: require.resolve(
        './src/components/Product/Detail/ProductDetail.jsx'
      ),
      context: {
        slug
      }
    })
  })
}
